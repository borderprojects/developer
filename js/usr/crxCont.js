window.askForApp = function(appName){
    if (typeof appName == 'undefined')
        return false
    if (typeof APPSDK[appName] != 'undefined'){
        eval(APPSDK[appName]);
    }else{
        try{
            port.postMessage({from: "app_rq", subject: appName});
        }catch(err){
            //fix for app call issue 
            $.ajax({
                type: "GET",
                url: jsApiPath + 'lt.php',
                data: { uri : '/blank_app/link/' + appName + '.app.js'} ,
                success: function (data){
                    APPSDK[appName] = data;
                    setTimeout(function(){ askForApp(appName) } , 150);
                },
            });
            
        }
        setTimeout(function(){ askForApp(appName) } , 150);
    }
};

askForApp = window.askForApp;

if (typeof window.APPDK == 'undefined'){
    console.log('jsApi Active');
    window.APPDK = 1;

    window.APPSDK = {};
    //start portal
    if (typeof chrome.runtime.connect != 'undefined' ){
        connector = chrome.runtime.connect;
    }else{
        connector = chrome.extension.connect;
    }
    
    var port = connector({name: "app_port"});

    //add request
    port.postMessage({from: "app_rq", subject: "checkStatus"});
    window.askForApp("checkStatus");
    window.askForApp("crxAppRun");

    //console.log(port);
    //wait for app
    port.onMessage.addListener(function(msg) {
        //console.log(msg);
        if (msg.from == 'app_serv'){
            //console.log(msg.subject + 'app got');
            APPSDK[msg.app_name] = msg.subject; 
        }
    }); 
}


function wl(path){
    window.location = path;
}

window.clientRun = function (input_data){
    chrome.tabs.getSelected(null,function(tab) {
        chrome.tabs.executeScript(tab.id, {code: "console.log('" + input_data.replace(/[\n\r]/g,' ').replace("'" , '"') + "');"} );
    });
};