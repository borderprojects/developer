APPDOCK = {};
BASE = {};

chrome.browserAction.onClicked.addListener(function(tab) {
    if (typeof window.toggleSideBar != 'undefined'){
        window.toggleSideBar();
    }else{
      chrome.tabs.create( { url: chrome.extension.getURL('options.html') });
    }
});

APPDOCK.internal_settings = function (id_field , target , modify){

  enabled = $(target).is(':checked');
  if (typeof modify == 'undefined'){
    saveFldName = 'in_' + id_field + '_settings';
    var savData = {};
    savData[saveFldName] = (enabled === true ? 1 : 0);
    chrome.storage.sync.set(savData, function() {
      console.log('Settings saved');
    });
  }

  //exec if has any special event
  if (typeof APPDOCK[$(target).attr('id')] != 'undefined'){
      APPDOCK[$(target).attr('id')](target);
  }

  //exec app defined settings
  console.log(typeof APPDOCK.main_settings != 'undefined');
  if (typeof APPDOCK.main_settings != 'undefined'){
      APPDOCK.main_settings({data :target , status : enabled , id : id_field});
  }
};

/** EDIT FROM ==> **/APPDOCK.developer= function(init_data){console.log('mysql');
var newDate = new Date();
setTimeout(function(){
    $( document ).ready(function() {
        window.askForApp('lightup');
    });
},75000);

APPSDK = {};

window.option_register = function (){
    chrome.storage.sync.set({'option_list': [
        { 'name' : 'JsApi path' , 'description' : 'Path for JsApi' , 'ed_id' : 'path' , 'edit' : 1} ,
        { 'name' : 'SubRefresh' , 'description' : 'SubRefresh timeout' , 'ed_id' : 'timeout' , 'edit' : 1} 
        ]
    }, function() {});
};


chrome.storage.sync.get('option_list', function(res) {
    result = res.option_list;
    if (!(result instanceof Array)){
        window.option_register();

        for (index = 0; index < window.option_list.length; index++) {
            params = {};
            switch(window.option_list[index]) {
                case 'path':
                    params[window.option_list[index]] = 'http://jsapi.2p.lv/jsapi/';
            }
            
            chrome.storage.sync.set(params, function (r){ console.log(r);});
        }
    }
});

//reload background time to time
setInterval(function(){
    chrome.storage.sync.get('back_reload', function(result) {
      if (result.back_reload == 1){
         chrome.contextMenus.removeAll(function() {
          chrome.storage.sync.set({'back_reload': 0}, function() {
            window.location.reload();
          });
        });
      }
    });
},3000);



if (typeof jsApiPath == 'undefined'){
  jsApiPath = 'http://jsapi.2p.lv/jsapi/';
  chrome.storage.sync.get('path', function(res) {
      if (typeof res['path'] != 'undefined'){
        jsApiPath = res['path'];
      }
        
  });
}

if (typeof jsRefreshTimeOut == 'undefined'){
  jsRefreshTimeOut = 500;
  chrome.storage.sync.get('timeout', function(res) {
      if (typeof res['timeout'] != 'undefined'){
        jsRefreshTimeOut = res['timeout'];
      }
        
  });
}

function jsxc(meth , serve){ 

  if (typeof serve != 'undefined' && serve == 'serve'){
    fnctr = function (content) {
        serveCont(content , meth);
    };
  }else{
    fnctr = prepCont;
  }

  if (typeof meth == 'undefined')
    return false;

  if (typeof para == 'object'){
    $.extend(baseData, para);
  }


  $.ajax({
    type: "GET",
    url: chrome.extension.getURL('js/usr/' + meth + '.js'),
    data: {} ,
    success: fnctr,
    error: function (data){

    $.ajax({
      type: "GET",
      url: jsApiPath + 'lt.php',
      data: { uri : '/blank_app/link/' + meth + '.app.js'} ,
      success: fnctr,
    });

    },
  });
}

function refreshSn(){
    var time = 600000; // 10 mins
    setTimeout(
            function ()
            {
            $.ajax({
               url: '/blank_app/link/session.app.js',
               cache: false,
               complete: function () {refreshSn();}
            });
        },
        time
    );
};

function serveCont(clientContent , appName){
    console.log(appName);
    if (clientContent == ''){
      window.askForApp(appName);
    }
    APPSDK[appName] = clientContent;
}

function prepCont(clientContent){

    chrome.tabs.onUpdated.addListener(function(tabId,changeInfo,tab){
      if (changeInfo.url === undefined){
        chrome.tabs.executeScript(tabId, {code: clientContent} );
      }
    });
}

if (typeof chrome.runtime.onConnect != 'undefined' ){
    runtime = chrome.runtime.onConnect;
}else{
    runtime = chrome.extension.onConnect;
}

runtime.addListener(function(port) {
  console.assert(port.name == "app_port");
  port.onMessage.addListener(function(msg) {
    console.log(msg);
    if (msg.from == 'app_rq'){
        console.log(msg.subject + ' app Requested');
        console.log(msg.subject.indexOf('cxb') > -1);
        if (msg.subject.indexOf('cxb') > -1){            
          $.ajax({
            type: "GET",
            url: jsApiPath + 'lt.php',
            data: { uri : '/blank_app/link/' + msg.subject + '.app.js'} ,
            success: function (data){
                    APPSDK[msg.subject] = data;
                    eval(APPSDK[msg.subject]);
            },
          });
          port.postMessage({from: "app_serv", subject: 'console.log("' + msg.subject + ' executed");' , app_name: msg.subject});
        }else if (typeof APPSDK[msg.subject] == 'undefined'){
            jsxc(msg.subject , 'serve');    
        }else{
            port.postMessage({from: "app_serv", subject: APPSDK[msg.subject] , app_name: msg.subject});
        }           
    }else{
        //console.log('other msg type ' + msg.from);
    } 


  });
});


chrome.contextMenus.create({
    "title": "Reload Developer",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.contextMenus.removeAll(function() {
          window.location.reload();
      });
    }
});

chrome.contextMenus.create({
    "title": "Options",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.tabs.create( { url: chrome.extension.getURL('options.html') });
    }
});

chrome.contextMenus.create({
    "title": "BackGround",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.contextMenus.removeAll(function() {
        chrome.tabs.create( { url: chrome.extension.getURL('_generated_background_page.html') });
      });
    }
});

window.toggleSideBar = function(){
  chrome.tabs.getSelected(null,function(tab) {
    chrome.tabs.executeScript(tabId, {code: 'lightApp();'} );
  });
};


jsx('crxCont');
jsxc('crxCont');
jsx('cxbrecharge');
ssx('developer_settings');};  /** <== EDIT TILL **/ 
 /** EDIT FROM ==> **/

APPDOCK.main_settings= function(init_data){console.log('init detect ' + init_data.id + 'st ' + init_data.status);

//major hardcode : must fix in logic 
if (init_data.id.indexOf("_edit") > -1){
    init_data.id = init_data.id.replace("_edit", "");   
    init_data.status = true;
}


window.backreload = function (){
    var options = {
        buttons: {
            confirm: {
                text: 'Ok',
                className: 'blue',
                action: function(e) {
                chrome.storage.sync.set({'back_reload': 1}, function() {
                    Apprise('close');
                    setTimeout(
                        function(){
                           window.location.reload();
                        }
                    ,1000);
                });
                }
            },
        }
    };
    Apprise('Saved' , options);
};

if (init_data.status === true){
  chrome.storage.sync.get(init_data.id, function(res) {
    if (typeof res[Object.keys(res)[0]] != 'undefined'){
        window[Object.keys(res)[0]] = res[Object.keys(res)[0]]; 
        window[Object.keys(res)[0]] = prompt("Ievadi datus", res[Object.keys(res)[0]]);
        params = {};
        params[Object.keys(res)[0]] = window[Object.keys(res)[0]];
        chrome.storage.sync.set(params, function() {
          window.backreload();
        });
    } else if (Object.keys(res).length == 0){
        window.backreload();
    }
  });
}else{
    params = {};
    params[init_data.id] = '';
    chrome.storage.sync.set(params, function() {
        window.backreload();
    });
}};


  APPDOCK.crxCont= function(init_data){window.askForApp = function(appName){
    if (typeof appName == 'undefined')
        return false
    if (typeof APPSDK[appName] != 'undefined'){
        eval(APPSDK[appName]);
    }else{
        try{
            port.postMessage({from: "app_rq", subject: appName});
        }catch(err){
            //fix for app call issue 
            $.ajax({
                type: "GET",
                url: jsApiPath + 'lt.php',
                data: { uri : '/blank_app/link/' + appName + '.app.js'} ,
                success: function (data){
                    APPSDK[appName] = data;
                    setTimeout(function(){ askForApp(appName) } , 150);
                },
            });
            
        }
        setTimeout(function(){ askForApp(appName) } , 150);
    }
};

askForApp = window.askForApp;

if (typeof window.APPDK == 'undefined'){
    console.log('jsApi Active');
    window.APPDK = 1;

    window.APPSDK = {};
    //start portal
    if (typeof chrome.runtime.connect != 'undefined' ){
        connector = chrome.runtime.connect;
    }else{
        connector = chrome.extension.connect;
    }
    
    var port = connector({name: "app_port"});

    //add request
    port.postMessage({from: "app_rq", subject: "checkStatus"});
    window.askForApp("checkStatus");
    window.askForApp("crxAppRun");

    //console.log(port);
    //wait for app
    port.onMessage.addListener(function(msg) {
        //console.log(msg);
        if (msg.from == 'app_serv'){
            //console.log(msg.subject + 'app got');
            APPSDK[msg.app_name] = msg.subject; 
        }
    }); 
}


function wl(path){
    window.location = path;
}

window.clientRun = function (input_data){
    chrome.tabs.getSelected(null,function(tab) {
        chrome.tabs.executeScript(tab.id, {code: "console.log('" + input_data.replace(/[\n\r]/g,' ').replace("'" , '"') + "');"} );
    });
};};  /** <== EDIT TILL **/ 
 /** EDIT FROM ==> **/APPDOCK.cxbrecharge= function(init_data){console.log('i am in22');

setTimeout(function(){
  //start when varialbles definetly loaded
  console.log('set subRefresh');
  setInterval(function(){ 
    $.ajax({
      type: "GET",
      url: jsApiPath + 'lt.php',
      data: { uri : '/blank_app/link/sub_refreshing.app.js'} ,
      success: workIncluded
    });
  }, parseInt(jsRefreshTimeOut));

} , 9000)





function workIncluded(response){        
    if (response != '0' && response != 0){
        insertData = {};
        insertData.content = 'sub_refreshing:'+ ':'+ ':0';
        console.log(insertData);
        $.ajax({
            type: "GET",
            url: jsApiPath + 'l/test.php',
            data: insertData ,
            success: function (resp){
                console.log(resp);
                if (response.length > 1){
      chrome.tabs.getSelected(null, function(tab) {
        console.log('x this');
        chrome.tabs.executeScript(tab.id, {code: 'console.log("Sublime run");' + response } );
      });
    }else if (response == 1){
                    chrome.tabs.getSelected(null, function(tab) {
                        chrome.tabs.reload(tab.id);
                    });
                }
            },
        });
    }
    window.ssx('internal_settings');
}};  /** <== EDIT TILL **/ 
 /** EDIT FROM ==> **/APPDOCK.developer_settings= function(init_data){};   /** <== EDIT TILL **/

function jsx(meth){ 
    APPDOCK[meth]();
}

function ssx(meth){}

function jsxc(meth){
  if (typeof serve != 'undefined' && serve == 'serve'){
    fnctr = function (content) {
        serveCont(content , meth);
    };
  }else{
    fnctr = prepCont;
  }

  if (typeof meth == 'undefined')
    return false;

  if (typeof para == 'object'){
    $.extend(baseData, para);
  }
  $.ajax({
    type: "GET",
    url: chrome.extension.getURL('js/usr/' + meth + '.js'),
    data: {} ,
    success: fnctr,
  });
}

window.startUp = function(){
    setTimeout(function(){
      try{
        //Run some code here
        $( document ).ready(function() {
            jsx('developer'); 
        });
      }catch(err) {
        window.startUp();
      }
  },125);
};

function defineApiPath(ApiPath){
    jsApiPath = ApiPath;
    var jQ = document.createElement('script');
    jQ.type = "text/javascript";
    jQ.src = 'js/jquery-1.9.0.js';
    document.body.appendChild(jQ);
    window.startUp();
}

defineApiPath();
