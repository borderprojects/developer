console.log('option init');
chrome.storage.sync.get('option_list', function(res) {
    result = res.option_list;
    if (result instanceof Array){
        //main option template
        var template = $('#template').html();
        Mustache.parse(template);   // optional, speeds up future uses

        //edit button template
        var edit_button_template = $('#edit_button_template').html();
        var index;
        for (index = 0; index < result.length; ++index) {

             var rendered = Mustache.render(template, 
             {
                name: result[index].name ,
                description: result[index].description,
                eid: result[index].ed_id,
                edit_button: (typeof result[index].edit == 'undefined' ? '' : Mustache.render(edit_button_template, {eid: result[index].ed_id}))
            });
            $('#extension-settings-list').append(rendered);
            //'in_' + id_field + '_settings'

            chrome.storage.sync.get('in_' + result[index].ed_id + '_settings', function(resin) {
                
                $.each(resin, function(index, value) {
                    
                    if (value == 1){
                        $('#' + index.replace("in_","").replace("_settings","")).prop( "checked", true );
                    }
                });               
            });

            $('#' + result[index].ed_id).bind('click', function(element) {
                if (typeof APPDOCK.internal_settings != 'undefined'){
                    APPDOCK.internal_settings($(element.target).attr('id') , element.target);
                }else{
                    console.log('No action for ' + $(element.target).attr('id'));
                }
            });

            $('#' + result[index].ed_id + '_edit').bind('click', function(element) {
                if (typeof APPDOCK.internal_settings != 'undefined'){
                    APPDOCK.internal_settings($(element.target).attr('id') , element.target , 1);
                }else{
                    console.log('No action for ' + $(element.target).attr('id'));
                }
            });

        }
    } 
});

